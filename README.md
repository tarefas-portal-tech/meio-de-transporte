# Meio de Transporte

variables

- Trator: caractere
- Moto: caractere
- Bicicleta: caractere
- Trem: caractere
- Carro: caractere
- Caminhão: caractere
- Ônibus: caractere
- Paraquedas: caractere
- Balão: caractere
- Avião: caractere
- Helicóptero: caractere
- Submarino: caractere
- Barco: caractere
- Navio: caractere
- Lancha: caractere
pergunta: caractere
resposta: caractere

escolha x

caso 1

digite(“é terrestre?”);
leia(sim);
digite(“cabe apenas uma pessoa?”);
leia(sim);
digite(“é pesado?”);
leia(sim);
digite(“tem asas?”);
leia(não);
digite(“ele navega?”);
leia(não);
resposta(Trator);

fimescolha

caso 2

digite(“é terrestre?”);
leia(sim);
digite(“cabe apenas uma pessoa?”);
leia(não);
digite(“é pesado?”);
leia(não);
digite(“tem asas?”);
leia(não);
digite(“ele navega?”);
leia(não);
resposta(Moto);


fimescolha

caso 3

digite(“é terrestre?”);
leia(sim);
digite(“cabe apenas uma pessoa?”);
leia(sim);
digite(“é pesado?”);
leia(não);
digite(“tem asas?”);
leia(não);
digite(“ele navega?”);
leia(não);
resposta(Bicicleta);

fimescolha

caso 4

digite(“é terrestre?”);
leia(sim);
digite(“cabe apenas uma pessoa?”);
leia(não);
digite(“é pesado?”);
leia(sim);
digite(“tem asas?”);
leia(não);
digite(“ele navega?”);
leia(não);
resposta(Trem);

fimescolha

caso 5

digite(“é terrestre?”);
leia(sim);
digite(“cabe apenas uma pessoa?”);
leia(não);
digite(“é pesado?”);
leia(sim);
digite(“tem asas?”);
leia(não);
digite(“ele navega?”);
leia(não);
resposta(Carro);

fimescolha

caso 6

digite(“é terrestre?”);
leia(sim);
digite(“cabe apenas uma pessoa?”);
leia(não);
digite(“é pesado?”);
leia(sim);
digite(“tem asas?”);
leia(não);
digite(“ele navega?”);
leia(não);
resposta(Caminhão);

fimescolha

caso 7

digite(“é terrestre?”);
leia(sim);
digite(“cabe apenas uma pessoa?”);
leia(não);
digite(“é pesado?”);
leia(sim);
digite(“tem asas?”);
leia(não);
digite(“ele navega?”);
leia(não);
resposta(Ônibus);

fimescolha

caso 8

digite(“é terrestre?”);
leia(não);
digite(“cabe apenas uma pessoa?”);
leia(sim);
digite(“é pesado?”);
leia(não);
digite(“tem asas?”);
leia(não);
digite(“ele navega?”);
leia(não);
resposta(Paraquedas);

fimescolha

caso 9

digite(“é terrestre?”);
leia(não);
digite(“cabe apenas uma pessoa?”);
leia(não);
digite(“é pesado?”);
leia(sim);
digite(“tem asas?”);
leia(não);
digite(“ele navega?”);
leia(não);
resposta(Balão);

fimescolha

caso 10

digite(“é terrestre?”);
leia(não);
digite(“cabe apenas uma pessoa?”);
leia(não);
digite(“é pesado?”);
leia(sim);
digite(“tem asas?”);
leia(sim);
digite(“ele navega?”);
leia(não);
resposta(Avião);

fimescolha

caso 11

digite(“é terrestre?”);
leia(não);
digite(“cabe apenas uma pessoa?”);
leia(não);
digite(“é pesado?”);
leia(não);
digite(“tem asas?”);
leia(não);
digite(“ele navega?”);
leia(não);
resposta(Helicóptero);

fimescolha

caso 12

digite(“é terrestre?”);
leia(não);
digite(“cabe apenas uma pessoa?”);
leia(não);
digite(“é pesado?”);
leia(sim);
digite(“tem asas?”);
leia(não);
digite(“ele navega?”);
leia(sim);
resposta(Submarino);

fimescolha

caso 13

digite(“é terrestre?”);
leia(não);
digite(“cabe apenas uma pessoa?”);
leia(não);
digite(“é pesado?”);
leia(sim);
digite(“tem asas?”);
leia(não);
digite(“ele navega?”);
leia(sim);
resposta(Barco);

fimescolha

caso 14

digite(“é terrestre?”);
leia(não);
digite(“cabe apenas uma pessoa?”);
leia(não);
digite(“é pesado?”);
leia(sim);
digite(“tem asas?”);
leia(não);
digite(“ele navega?”);
leia(sim);
resposta(Navio);

fimescolha

outro caso

digite(“é terrestre?”);
leia(não);
digite(“cabe apenas uma pessoa?”);
leia(não);
digite(“é pesado?”);
leia(não);
digite(“tem asas?”);
leia(não);
digite(“ele navega?”);
leia(sim);
resposta(Lancha);

fimescolha

